package com.claragoncalves.contactlist.model.dao;


import android.os.AsyncTask;

import com.claragoncalves.contactlist.model.pojo.Contact;
import com.claragoncalves.contactlist.model.pojo.ContactContainer;
import com.claragoncalves.contactlist.util.DAOException;
import com.claragoncalves.contactlist.util.HTTPConnectionManager;
import com.claragoncalves.contactlist.util.ResultListener;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Clara on 17/01/2018.
 */

public class ContactDao {

    private static final String contactsUrl = "https://s3.amazonaws.com/technical-challenge/v3/contacts.json";

    public void getContacts(ResultListener<List<Contact>> listener) {
        RetrieveContactsTask retrieveContactsTask = new RetrieveContactsTask(listener);
        retrieveContactsTask.execute(contactsUrl);
    }

    public class RetrieveContactsTask extends AsyncTask<String, Void, List<Contact>> {
        ResultListener<List<Contact>> resultListener;


        public RetrieveContactsTask(ResultListener<List<Contact>> resultListener) {
            this.resultListener = resultListener;
        }

        @Override
        protected List<Contact> doInBackground(String... strings) {
            HTTPConnectionManager connectionManager = new HTTPConnectionManager();
            String link = strings[0];
            String input = null;

            try {
                input = connectionManager.getRequestString(link);
            } catch (DAOException e) {
                e.printStackTrace();
            }

            Gson gson = new Gson();
            ContactContainer container = gson.fromJson(input, ContactContainer.class);

            return container;
        }

        @Override
        protected void onPostExecute(List<Contact> contacts) {
            resultListener.finish(contacts);

        }
    }
}
