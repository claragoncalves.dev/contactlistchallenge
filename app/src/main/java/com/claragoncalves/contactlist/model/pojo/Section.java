package com.claragoncalves.contactlist.model.pojo;

import java.util.List;

/**
 * Created by Clara on 17/01/2018.
 */

public class Section {
    private String header;
    private List<Contact> contacts;

    public Section(String header, List<Contact> contacts) {
        this.header = header;
        this.contacts = contacts;
    }

    public String getHeader() {
        return header;
    }

    public List<Contact> getContacts() {
        return contacts;
    }
}
