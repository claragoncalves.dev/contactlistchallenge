package com.claragoncalves.contactlist.model.pojo;



/**
 * Created by Clara on 17/01/2018.
 */

public class Contact{
    private String name;
    private String id;
    private String companyName;
    private Boolean isFavorite;
    private String smallImageURL;
    private String largeImageURL;
    private String emailAddress;
    private String birthdate;
    private Phone phone;
    private Address address;

    public String getSmallImageURL() {
        return smallImageURL;
    }

    public String getName() {
        return name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Boolean getFavorite() {
        return isFavorite;
    }

    public String getId() {
        return id;
    }

    public void setFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public String getLargeImageURL() {
        return largeImageURL;
    }

    public Phone getPhone() {
        return phone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public Address getAddress() {
        return address;
    }

    public class Address{
        private String street;
        private String city;
        private String state;
        private String country;
        private String zipCode;

        public String getCity() {
            return city;
        }

        public String getCountry() {
            return country;
        }

        public String getState() {
            return state;
        }

        public String getStreet() {
            return street;
        }

        public String getZipCode() {
            return zipCode;
        }
    }

    public class Phone{
        private String work;
        private String home;
        private String mobile;

        public String getHome() {
            return home;
        }

        public String getMobile() {
            return mobile;
        }

        public String getWork() {
            return work;
        }
    }

}
