package com.claragoncalves.contactlist.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.claragoncalves.contactlist.R;
import com.claragoncalves.contactlist.adapter.SectionAdapter;
import com.claragoncalves.contactlist.controller.ContactController;


public class ContactListFragment extends Fragment{

    private RecyclerView recyclerView;
    private SectionAdapter sectionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        recyclerView = view.findViewById(R.id.recyclerviewContacts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        loadContacts();
        return view;
    }

    private void loadContacts(){
        sectionAdapter = new SectionAdapter(ContactController.getInstance().getSections(getContext()));
        recyclerView.setAdapter(sectionAdapter);
    }

    public void updateContacts(String contactId){
        sectionAdapter.setSections(ContactController.getInstance().updateContact(contactId, getContext()));
    }
}
