package com.claragoncalves.contactlist.view;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.claragoncalves.contactlist.R;
import com.claragoncalves.contactlist.adapter.ContactAdapter;
import com.claragoncalves.contactlist.controller.ContactController;
import com.claragoncalves.contactlist.model.pojo.Section;
import com.claragoncalves.contactlist.util.ResultListener;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ContactAdapter.ContactClickListener, ContactDetailFragment.ContactFavoriteListener{

    private static final String FRAGMENT_LIST = "fragmentList";
    private static final String FRAGMENT_DETAIL = "fragmentDetail";
    private ContactListFragment contactListFragment;
    private ContactDetailFragment contactDetailFragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.appBar);
        setSupportActionBar(toolbar);
        contactListFragment = new ContactListFragment();
        contactDetailFragment = new ContactDetailFragment();
        ContactController.getInstance().getContacts(new ResultListener<List<Section>>() {
            @Override
            public void finish(List<Section> result) {
                placeFragment(contactListFragment, FRAGMENT_LIST);
            }
        }, this);
    }

    private void placeFragment(Fragment fragment, String id){
        if (id.equals(FRAGMENT_LIST)){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolderMain, fragment, id).commit();
        }else{
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolderMain, fragment, id).addToBackStack(null).commit();
        }
    }

    @Override
    public void contactClicked(String id) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ContactDetailFragment.CONTACT_KEY, id);
        contactDetailFragment.setArguments(bundle);
        placeFragment(contactDetailFragment, FRAGMENT_DETAIL);
    }

    @Override
    public void contactFavClicked(String contactId) {
        contactListFragment.updateContacts(contactId);
        switchFavImage(contactId);
    }

    private void switchFavImage(String contactId){
        if (ContactController.getInstance().getContact(contactId).getFavorite()){
            toolbar.getMenu().getItem(0).setIcon(R.drawable.fav_true);
        }else {
            toolbar.getMenu().getItem(0).setIcon(R.drawable.fav_false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getSupportFragmentManager().popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
