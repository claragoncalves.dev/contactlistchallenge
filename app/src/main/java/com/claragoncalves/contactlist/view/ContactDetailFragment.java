package com.claragoncalves.contactlist.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.claragoncalves.contactlist.R;
import com.claragoncalves.contactlist.controller.ContactController;
import com.claragoncalves.contactlist.model.pojo.Contact;
import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class ContactDetailFragment extends Fragment{
    public static final String CONTACT_KEY = "contactKey";
    private Contact contact;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_contact_detail, container, false);
        contact = ContactController.getInstance().getContact(getArguments().getString(CONTACT_KEY));
        setupViews();
        return view;
    }

    private void setupViews(){
        setActionBar();
        ImageView imageViewContact = view.findViewById(R.id.imageviewContactDetailImage);
        RequestOptions requestOptions = new RequestOptions().error(R.drawable.user_small).placeholder(R.drawable.user_small);
        Glide.with(getContext()).load(contact.getLargeImageURL()).apply(requestOptions).transition(withCrossFade()).into(imageViewContact);
        TextView textViewName = view.findViewById(R.id.textviewContactDetailName);
        textViewName.setText(contact.getName());

        TextView textViewCompany = view.findViewById(R.id.textviewContactDetailCompany);
        if (ContactController.getInstance().checkContent(contact.getCompanyName())) {
            textViewCompany.setText(contact.getCompanyName());
        }else {
            textViewCompany.setVisibility(View.GONE);
        }
        if (ContactController.getInstance().checkContent(contact.getPhone().getHome())) {
            TextView textViewPhoneHome = view.findViewById(R.id.textviewContactDetailPhoneHome);
            textViewPhoneHome.setText(contact.getPhone().getHome());
        }else {
            LinearLayout layoutPhoneHome = view.findViewById(R.id.layoutGroupContactDetailPhoneHome);
            layoutPhoneHome.setVisibility(View.GONE);
        }
        if (ContactController.getInstance().checkContent(contact.getPhone().getMobile())) {
            TextView textViewPhoneMobile = view.findViewById(R.id.textviewContactDetailPhoneMobile);
            textViewPhoneMobile.setText(contact.getPhone().getMobile());
        }else {
            LinearLayout layoutPhoneMobile = view.findViewById(R.id.layoutGroupContactDetailPhoneMobile);
            layoutPhoneMobile.setVisibility(View.GONE);
        }
        if (ContactController.getInstance().checkContent(contact.getPhone().getWork())) {
            TextView textViewPhoneWork = view.findViewById(R.id.textviewContactDetailPhoneWork);
            textViewPhoneWork.setText(contact.getPhone().getWork());
        }else {
            LinearLayout layoutPhoneWork = view.findViewById(R.id.layoutGroupContactDetailPhoneWork);
            layoutPhoneWork.setVisibility(View.GONE);
        }
        if (ContactController.getInstance().checkContent(contact.getEmailAddress())){
            TextView textViewEmail = view.findViewById(R.id.textviewContactDetailEmail);
            textViewEmail.setText(contact.getEmailAddress());
        }else {
            LinearLayout layoutEmail = view.findViewById(R.id.layoutGroupContactDetailEmail);
            layoutEmail.setVisibility(View.GONE);
        }
        if (ContactController.getInstance().checkContent(contact.getBirthdate())){
            TextView textViewBirthdate = view.findViewById(R.id.textviewContactDetailBirthdate);
            textViewBirthdate.setText(contact.getBirthdate());
        }else {
            LinearLayout layoutBirthdate = view.findViewById(R.id.layoutGroupContactDetailBirthdate);
            layoutBirthdate.setVisibility(View.GONE);
        }
        if (ContactController.getInstance().checkContent(contact.getAddress().getStreet())){
            String address = contact.getAddress().getStreet() + getContext().getResources().getString(R.string.address_separator) + contact.getAddress().getCity() + getContext().getResources().getString(R.string.address_separator) + contact.getAddress().getState() + getContext().getResources().getString(R.string.address_separator) + contact.getAddress().getCountry();
            TextView textViewAddress = view.findViewById(R.id.textviewContactDetailAddress);
            textViewAddress.setText(address);
            TextView textViewZip = view.findViewById(R.id.textviewContactDetailAddressZip);
            textViewZip.setText(contact.getAddress().getZipCode());
        }else {
            LinearLayout layoutAddress = view.findViewById(R.id.layoutGroupContactDetailAddress);
            layoutAddress.setVisibility(View.GONE);
        }
    }

    private void setActionBar(){
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.app_bar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        switchFavImage(menu);
    }

    public interface ContactFavoriteListener{
        void contactFavClicked(String contactId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.buttonFav:
                switchFav();
                return true;
            default:
                return false;
        }
    }

    private void switchFav(){
        ContactFavoriteListener contactFavoriteListener = (ContactFavoriteListener) getContext();
        contactFavoriteListener.contactFavClicked(contact.getId());
    }

    public void switchFavImage(Menu menu){
        if (ContactController.getInstance().getContact(contact.getId()).getFavorite()){
            menu.getItem(0).setIcon(R.drawable.fav_true);
        }else {
            menu.getItem(0).setIcon(R.drawable.fav_false);
        }
    }
}
