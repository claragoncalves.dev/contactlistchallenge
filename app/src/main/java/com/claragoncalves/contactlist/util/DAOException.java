package com.claragoncalves.contactlist.util;

/**
 * Created by Clara on 08/08/2017.
 */

public class DAOException extends Exception{
    public DAOException(String error){
        super(error);
    }
}