package com.claragoncalves.contactlist.util;

/**
 * Created by Clara on 17/01/2018.
 */

public interface ResultListener<T> {
    void finish(T result);
}
