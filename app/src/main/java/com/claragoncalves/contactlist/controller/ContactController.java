package com.claragoncalves.contactlist.controller;

import android.content.Context;

import com.claragoncalves.contactlist.R;
import com.claragoncalves.contactlist.model.dao.ContactDao;
import com.claragoncalves.contactlist.model.pojo.Contact;
import com.claragoncalves.contactlist.model.pojo.Section;
import com.claragoncalves.contactlist.util.ResultListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Clara on 17/01/2018.
 */

public class ContactController {
    private static ContactController instance = null;
    private List<Contact> contacts;

    protected ContactController() {
    }

    public static ContactController getInstance() {
        if(instance == null) {
            instance = new ContactController();
        }
        return instance;
    }

    public void getContacts(final ResultListener<List<Section>> listener, final Context context) {
        ContactDao contactDao = new ContactDao();
        contactDao.getContacts(new ResultListener<List<Contact>>() {
            @Override
            public void finish(List<Contact> result) {
                if (result!=null) {
                    contacts = result;
                    listener.finish(sortContactsInSections(result, context));
                }
            }
        });
    }

    public List<Section> getSections(Context context){
        return sortContactsInSections(contacts, context);
    }

    public List<Section> updateContact(String id, Context context){
        for (Contact contact:contacts) {
            if (contact.getId().equals(id)){
                contact.setFavorite(!contact.getFavorite());
                break;
            }
        }
        return sortContactsInSections(contacts, context);
    }

    public Contact getContact(String id){
        Contact contactSearched = null;
        for (Contact contact:contacts) {
            if (contact.getId().equals(id)){
                contactSearched = contact;
            }
        }
        return contactSearched;
    }

    public Boolean checkContent(String data){
        if (data!=null){
            if (data.length()>0){
                return true;
            }
        }
        return false;
    }

    public List<Section> sortContactsInSections(List<Contact> contacts, Context context) {
        List<Section> sections = new ArrayList<>();
        List<Contact> favContacts = new ArrayList<>();
        List<Contact> otherContacts = new ArrayList<>();
        for (Contact contact : contacts) {
            if (contact.getFavorite()) {
                favContacts.add(contact);
            } else {
                otherContacts.add(contact);
            }
        }
        if (favContacts.size() > 0) {
            sortContactsByName(favContacts);
            sections.add(new Section(context.getString(R.string.favorite_contacts), favContacts));
        }
        if (otherContacts.size() > 0) {
            sortContactsByName(otherContacts);
            sections.add(new Section(context.getString(R.string.other_contacts), otherContacts));
        }
        return sections;
    }

    private void sortContactsByName(List<Contact> result) {
        Collections.sort(result, new Comparator<Contact>() {
            @Override
            public int compare(Contact contact, Contact t1) {
                return contact.getName().compareTo(t1.getName());
            }
        });
    }

}
