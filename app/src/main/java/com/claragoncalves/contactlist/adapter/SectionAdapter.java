package com.claragoncalves.contactlist.adapter;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.claragoncalves.contactlist.R;
import com.claragoncalves.contactlist.model.pojo.Section;

import java.util.List;

/**
 * Created by Clara on 17/01/2018.
 */

public class SectionAdapter extends RecyclerView.Adapter{
    List<Section> sections;

    public SectionAdapter(List<Section> sections) {
        this.sections = sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SectionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.section_recycler_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
        sectionViewHolder.bindSection(sections.get(position));
    }

    @Override
    public int getItemCount() {
        return sections.size();
    }

    private class SectionViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewSectionTitle;
        private RecyclerView recyclerViewSection;

        private SectionViewHolder(View itemView) {
            super(itemView);
            textViewSectionTitle = itemView.findViewById(R.id.textViewHeaderTitle);
            recyclerViewSection = itemView.findViewById(R.id.recyclerviewSection);
            recyclerViewSection.setLayoutManager(new LinearLayoutManager(itemView.getContext(),LinearLayoutManager.VERTICAL, false));
            recyclerViewSection.addItemDecoration(new DividerItemDecorator(itemView.getContext().getDrawable(R.drawable.divider)));
        }

        private void bindSection(Section section){
            textViewSectionTitle.setText(section.getHeader());
            recyclerViewSection.setAdapter(new ContactAdapter(section.getContacts()));
        }
    }

    public class DividerItemDecorator extends RecyclerView.ItemDecoration {
        private Drawable mDivider;

        public DividerItemDecorator(Drawable divider) {
            mDivider = divider;
        }

        @Override
        public void onDraw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
            int dividerLeft = parent.getPaddingLeft();
            int dividerRight = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i <= childCount - 2; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int dividerTop = child.getBottom() + params.bottomMargin;
                int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();

                mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
                mDivider.draw(canvas);
            }
        }
    }
}
