package com.claragoncalves.contactlist.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.claragoncalves.contactlist.R;
import com.claragoncalves.contactlist.model.pojo.Contact;

import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Clara on 17/01/2018.
 */

public class ContactAdapter extends RecyclerView.Adapter {
    private List<Contact> contacts;
    private ContactClickListener contactClickListener;

    public ContactAdapter(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ContactViewHolder contactViewHolder = (ContactViewHolder) holder;
        contactViewHolder.bindContact(contacts.get(position));
        contactViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactClickListener = (ContactClickListener) view.getContext();
                contactClickListener.contactClicked(contacts.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }


    private class ContactViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageViewContact;
        private ImageView imageViewFav;
        private TextView textViewContactName;
        private TextView textViewContactCompanyName;
        private RequestOptions requestOptions;

        private ContactViewHolder(View itemView) {
            super(itemView);
            imageViewContact = itemView.findViewById(R.id.contactCellImageView);
            imageViewFav = itemView.findViewById(R.id.favImageView);
            textViewContactName = itemView.findViewById(R.id.contactCellTextViewName);
            textViewContactCompanyName = itemView.findViewById(R.id.contactCellTextViewCompany);
            requestOptions = new RequestOptions().error(R.drawable.user_small).placeholder(R.drawable.user_small);
        }

        private void bindContact(Contact contact) {
            setFavIcon(contact.getFavorite());
            Glide.with(itemView.getContext()).load(contact.getSmallImageURL()).apply(requestOptions).transition(withCrossFade()).into(imageViewContact);
            textViewContactName.setText(contact.getName());
            textViewContactCompanyName.setText(contact.getCompanyName());
        }

        private void setFavIcon(Boolean state) {
            if (state) {
                imageViewFav.setImageResource(R.drawable.fav_true);
            } else {
                imageViewFav.setImageDrawable(null);
            }
        }
    }

    public interface ContactClickListener{
        void contactClicked(String id);
    }
}
